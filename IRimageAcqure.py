import pyrealsense2 as rs
import numpy as np
import cv2

try:
    # Create a context object. This object owns the handles to all connected realsense devices
    pipeline = rs.pipeline()

    # Configure streams
    config = rs.config()
    #config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
    config.enable_stream(rs.stream.infrared, 1, 1280,720, rs.format.y8, 30)
    config.enable_stream(rs.stream.infrared, 2, 1280,720, rs.format.y8, 30)
    #옵션을 체크하기 위한 예제
    #config.enable_stream(stream_type=rs.stream.color, width = 1280, height = 720, format = rs.format.rgb8, framerate = 30)
    #config.enable_stream(stream_type=rs.stream.depth, width = 848, height = 480, format = rs.format.z16,framerate = 30)
    #config.enable_stream(stream_type=rs.stream.infrared, stream_index = 1, width = 848, height = 480, format = rs.format.y8,framerate = 30)
    #config.enable_stream(stream_type=rs.stream.infrared,stream_index = 2, width = 848, height = 480, format = rs.format.y8,framerate = 30)


    # Start streaming
    pipeline.start(config)

    while True:
        frames = pipeline.wait_for_frames()
        Left_frame = frames.get_infrared_frame(1) # 내가 봤을때 왼쪽
        Right_frame = frames.get_infrared_frame(2)

        if not Left_frame or not Right_frame:
            continue

        Infra_Left_image = np.asanyarray(Left_frame.get_data())
        Infra_Right_image = np.asanyarray(Right_frame.get_data())


        images = np.hstack((Infra_Left_image, Infra_Right_image))

        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', images)
        key = cv2.waitKey(1)

        if key & 0xFF == ord('q') or key == 27:
            cv2.destroyAllWindows()
            break

    exit(0)


except Exception as e:
    print(e)
    pass

finally:
    pipeline.stop()