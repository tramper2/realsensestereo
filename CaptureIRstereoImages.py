import pyrealsense2 as rs
import numpy as np
import cv2
import time
from datetime import datetime


total_photos = 30             # Number of images to take
countdown = 5                 # Interval for count-down timer, seconds
font=cv2.FONT_HERSHEY_SIMPLEX # Cowntdown timer font

try:
    # Create a context object. This object owns the handles to all connected realsense devices
    pipeline = rs.pipeline()

    # Configure streams
    config = rs.config()
    config.enable_stream(rs.stream.infrared, 1, 1280,720, rs.format.y8, 30)
    config.enable_stream(rs.stream.infrared, 2, 1280,720, rs.format.y8, 30)


    # Start streaming
    pipeline.start(config)

    counter = 0
    t2 = datetime.now()

    while True:
        frames = pipeline.wait_for_frames()
        Left_frame = frames.get_infrared_frame(1) # 내가 봤을때 왼쪽
        Right_frame = frames.get_infrared_frame(2)

        if not Left_frame or not Right_frame:
            continue

        Infra_Left_image = np.asanyarray(Left_frame.get_data())
        Infra_Right_image = np.asanyarray(Right_frame.get_data())


        images = np.hstack((Infra_Left_image, Infra_Right_image))

        t1 = datetime.now()
        cntdwn_timer = countdown - int ((t1-t2).total_seconds())
        # If cowntdown is zero - let's record next image
        if cntdwn_timer == -1:
            counter += 1
            Leftfilename = './scenes/leftImage_'+str(640)+'x'+str(480)+'_'+\
                        str(counter) + '.png'
            cv2.imwrite(Leftfilename, Infra_Left_image)
            Rightfilename = './scenes/rightImage_'+str(640)+'x'+str(480)+'_'+\
                        str(counter) + '.png'
            cv2.imwrite(Rightfilename, Infra_Right_image)


            print (' ['+str(counter)+' of '+str(total_photos)+'] '+ Leftfilename)
            t2 = datetime.now()
            time.sleep(1)
            cntdwn_timer = 0      # To avoid "-1" timer display 
            next
        # Draw cowntdown counter, seconds
        cv2.putText(images, str(cntdwn_timer), (50,50), font, 2.0, (0,0,255),4, cv2.LINE_AA)

        cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
        cv2.imshow('RealSense', images)
        key = cv2.waitKey(1)

        if key & 0xFF == ord('q') or key == 27:
            cv2.destroyAllWindows()
            break

    exit(0)


except Exception as e:
    print(e)
    pass

finally:
    pipeline.stop()